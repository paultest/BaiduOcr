<?php

/**
 * 上传文件
 * @param array $fileInfo 图片信息
 * @param string $path 路径
 * @param array $allowExt 可允许扩展名
 * @param int $maxSize 最大文件空间
 * @param bool $flag 是否检测真实图片类型
 * @return string 返回文件地址
 * @throws Exception
 */
function uploadFile($fileInfo, $path = 'uploads', $allowExt = ['jpeg', 'jpg', 'gif', 'png', 'bmp'], $maxSize = 1024 * 1024 * 7,  $flag = true)
{
    if (!is_array($allowExt)) {
        throw new Exception('参数错误');
    }

    // 上传文件的错误号(无错误则为0)
    $error = $fileInfo['error'];

    if ($error > 0) {
        // 匹配错误信息
        switch ($error) {
            case 1:
                $mes = '上传文件超过PHP配置文件中upload_max_filesize选项的值';
                break;
            case 2:
                $mes = '超过了表单MAX_FILE_SIZE限制的大小';
                break;
            case 3:
                $mes = '文件部分被上传';
                break;
            case 4:
                $mes = '没有选择上传文件';
                break;
            case 6:
                $mes = '没有找到临时目录';
                break;
            case 7:
                $mes = '文件写入失败';
                break;
            case 8:
                $mes = '上传文件被PHP扩展程序中断';
                break;
            default:
                $mes = '';
                break;
        }
        throw new Exception($mes);
    }

    // 判断上传文件的大小
    if ($fileInfo['size'] > $maxSize) {
        throw new Exception('上传文件过大，超出了最大文件大小' . $maxSize);
    }

    // 上传文件名
    $filename = $fileInfo['name'];

    // 文件的扩展名
    $ext = pathinfo($filename, PATHINFO_EXTENSION);

    // 判断文件类型
    if (!in_array($ext, $allowExt)) {
        throw new Exception('非法文件类型');
    }

    // 上传文件的临时路径
    $tmp_name = $fileInfo['tmp_name'];

    // 判断文件是否通过HTTP_POST方式上传来的
    if (!is_uploaded_file($tmp_name)) {
        throw new Exception('文件不是通过HTTP_POST方式上传来的');
    }

    // 检测是否为真实图片
    if ($flag) {
        if (!getimagesize($tmp_name)) {
            throw new Exception('文件不是真实图片类型');
        }
    }

    // 判断目录是否存在，如果不存在
    if (!file_exists($path)) {
        mkdir($path, 0777, true);
        chmod($path, 0777);
    }

    // 上传的文件名,确保文件名唯一，防止重名产生覆盖
    $destination = $path . '/' . md5(uniqid(microtime(true), true)) . '.' . $ext;

    // 将服务器上的临时文件移动到指定的目录下
    if (move_uploaded_file($tmp_name, $destination)) {
        return $destination;
    } else {
        throw new Exception('文件上传失败');
    }
}