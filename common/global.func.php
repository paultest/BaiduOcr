<?php

/**
 * 判断请求类型是否为post类型
 * @return bool
 */
function isPost()
{
    return isset($_SERVER['REQUEST_METHOD']) && strtoupper($_SERVER['REQUEST_METHOD']) == 'POST';
}

/**
 * 返回格式的处理
 * @param $type
 * @param $res
 * @return array|string
 */
function handleResult($type, $res)
{
    $result = [];
    switch ($type) {
        case 1:
        case 2:
        case 3:
            $words = '';
            if (isset($res['words_result'])) {
                foreach ($res['words_result'] as $v) {
                    $words .= $v['words'] . "<br />";
                }
            }
            $result['data'] = $words;
            $result['status'] = 1;
            break;
        case 4:
        case 6:
        case 7:
        case 9:
        case 10:
            foreach ($res['words_result'] as $k => $v) {
                $result['data'][$k] = $v['words'];
            }
            $result['status'] = 1;
            break;
        case 5:
            $result['data'] = isset($res['result']) ? $res['result'] : '';
            $result['status'] = 1;
            break;
        case 8:
            $result['data'] = isset($res['words_result']) ? $res['words_result'] : '';
            $result['status'] = 1;
            break;
        default:
            $result = [];
            break;
    }
    return $result;
}
