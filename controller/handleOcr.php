<?php
/**
 * 文字识别
 *      通用文字识别
 *      通用文字识别（高精度）
 *      网络图片文字识别
 *      身份证识别
 *      银行卡识别
 *      驾驶证识别
 *      行驶证识别
 *      车牌识别
 *      营业执照识别
 *      通用票据识别
 */
require_once dirname(__DIR__) . '/controller/common.php';

try {
    $type = intval($_POST['type']);

    // 处理图片
    $result = ImageHanler::handleOcr($_FILES['myFile'], UPLOAD_PATH, $type);

    echo json_encode(['status' => 1, 'data' => $result]);
    exit();
} catch (Exception $e) {
    echo json_encode(['data' => $e->getMessage(), 'status' => 0]);
    exit();
}
