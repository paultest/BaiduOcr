<?php
/**
 * 图像识别
 *      菜品识别
 *      车辆识别
 *      logo商标识别
 *      动物识别
 *      植物识别
 */
require_once dirname(__DIR__) . '/controller/common.php';

try {
    $type = intval($_POST['type']);

    // 处理图片
    $result = ImageHanler::handleImage($_FILES['myFile'], UPLOAD_PATH, $type);

    echo json_encode(['status' => 1, 'data' => $result]);
    exit();
} catch (Exception $e) {
    echo json_encode(['data' => $e->getMessage(), 'status' => 0]);
    exit();
}
