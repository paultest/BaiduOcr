<?php

header('content-type:text/html;charset=utf-8');

// 所有被包含文件均须使用此常量确定路径
define('ROOT_PATH', realpath(dirname(dirname(__FILE__))) . '/');

// 上传图片的路径
define('UPLOAD_PATH', ROOT_PATH . 'uploads');

require_once ROOT_PATH . 'config/baiduyun.php';
require_once ROOT_PATH . 'common/global.func.php';
require_once ROOT_PATH . 'common/files.func.php';
require_once ROOT_PATH . 'controller/ImageHanler.php';